/* eslint-disable shopify/jquery-dollar-sign-reference */
/* eslint-disable no-self-assign */
/* eslint-disable eqeqeq */
/**
 * Collection methods
 * -----------------------------------------------------------------------------
 *
 * @namespace Listcollection
 */

import axios from 'axios';
import Flickity from 'flickity';
import detectIt from 'detect-it';

const createHistory = require('history').createBrowserHistory;

const history = createHistory();

const collectionListMethods = {

  data() {
    return {
      isCollectionAjaxed: false,
      collections_col: {
        collection: [],
        pagination: {},
      },
    };
  },

  methods: {
    _initListCollection() {
      // stop parsing if we are not in the collection template
      if (app.currentTemplate !== 'list_collection') {
        return;
      }

      $('[data-collections-coll-json]').each((ind, item) => {
        const collObj = $(item).html();
        const collData = JSON.parse(collObj);
        app.collections_col.collection.push(collData);
      });

      Shopify.queryParams = {};

      let url = app._CreateUrl();
      url += 'view=collections.json';

      app._getListcollectionPagination(url);
      app._initCarouselsList();
    },

    _initCarouselsList() {
      // init list-collection carousel
      setTimeout(() => {
        const $carousel = $('[data-flickity]');
        $($carousel).each((_index, ele) => {
          ele = $(ele);
          if (ele.length > 0) {
            const options = ele.data('flickity');
            const flkty = new Flickity(ele[0], options);
            flkty.resize();
          }
        });
      }, 1000);
    },

    _CreateUrl(e) {
      const b = $.param(Shopify.queryParams).replace(/%2B/g, '+');
      return e ? b !== '' ? `${e}?${b}` : e : `${location.pathname}?${b}`;
    },

    _getListcollectionPagination(url) {
      url += '&view=paginate.json';

      axios.get(url)
        .then((response) => {
          app.collections_col.pagination = response.data;
          return response;
        })
        .catch((error) => {
          // console.log(error);
          throw error;
        });
    },

    _loadMoreList(event) {
      let url = $(event.currentTarget).attr('href');
      url = url.replace('paginate', 'collections');

      app.isLoading = true;
      axios.get(url)
        .then((response) => {
          $.each(response.data, (key, item) => {
            if (item.products.length > 0) {
              app.collections_col.collection.push(item);
            }
          });

          app.isLoading = false;
          app._getListcollectionPagination(url);
          app._initCarouselsList();
          return response;
        })
        .catch((error) => {
          // console.log(error);
          throw error;
        });
    },

  },
};

export default collectionListMethods;
