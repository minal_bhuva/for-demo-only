/* eslint-disable no-empty */
/* eslint-disable promise/catch-or-return */
/* eslint-disable shopify/jquery-dollar-sign-reference */
/* eslint-disable prefer-template */
/* eslint-disable promise/always-return */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable newline-per-chained-call */
/**
 * Custom methods
 * -----------------------------------------------------------------------------
 *
 * @namespace custom
 */

// media queries in Javascript => http://wicky.nillia.ms/enquire.js/
import enquire from 'enquire.js';
// detect if a device is mouseOnly, touchOnly, or hybrid => http://detect-it.rafrex.com/
import detectIt from 'detect-it';
import Flickity from 'flickity';
import 'flickity-as-nav-for';
import 'flickity-fullscreen';
import subscribe from 'klaviyo-subscribe';
import axios from 'axios';

const createHistory = require('history').createBrowserHistory;

const history = createHistory();

const customMethods = {

  data() {
    return {
      // your data here
      breadcrumb: {
        nav: '',
      },
      customerEmail: '',
    };
  },

  methods: {
    _initCustomMethods() {
      // custom JS code
      app._footer();
      app._prodVendor();
      app._flickitySlider();
      app._productOption();
      app._sidebarFilter();
      app._addLable();
      app._VIP();


      if ($( '.shopify-brand-collection' ).length ) {
        $('.main-slider-vertical').slick({
          vertical: true,
          verticalSwiping: true,
          slidesToShow: 1,
          arrows: true,
          dots: true,
          slidesToScroll: 1
        });
      }


    },

    _updateCustomerTags(emailId, tagsData) {
      const data = {
        email: emailId,
        customer_tags: tagsData,
      };
      console.log(data)
      $.ajax({
        type: 'POST',
        url: 'https://script.praella.com/ruze-retainer/update_customer_tag_api.php',
        dataType: 'json',
        data,
        success(data) {
          console.log('Updted Successfully')
          return data;
        },
        error(res) {
          console.log('Error=>', res);
        },
      });
    },

    _createCustomerCheck(emailId) {
      const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      if (emailId === '' || emailId.length <= 0) {
        $('[data-customercheck]').find('[data-subscribeError]').html("Email Address can't be empty.").css('color', '#ff1100').show();
        return;
      } else if (re.test(emailId) === false) {
        $('[data-customercheck]').find('[data-subscribeError]').html('Please provide valid Email Address.').css('color', '#ff1100').show();
        return;
      }
      $('[data-customercheck]').find('[data-subscribeError]').html('').hide();
      const data = {
        email: emailId,
      };
      $.ajax({
        type: 'POST',
        url: 'https://script.praella.com/ruze-retainer/create_customer_api.php',
        dataType: 'json',
        data,
        success(data) {
          if (data.status === '1') {
            $('[data-customercheck]').hide();
            $('[data-FormDiv]').find('[name="email"]').val(emailId);
            $('[data-FormDiv]').removeClass('d-none');
          }
          return data;
        },
        error(res) {
          $('[data-customercheck]').find('[data-subscribeError]').html('Something Went Wrong! Please try again.').css('color', '#ff1100').show();
          console.log(res);
        },
      });
    },

    _VIP() {
     // console.log($('.ResetElements__Form-sc-8e6zl9-1 button').length);
     // $('body').on('click', '.ResetElements__Form-sc-8e6zl9-1 button', function() {
      console.log('kalviyo button');
      console.log($('.klaviyo-form-KAF3vb button').length);
      $('body').on('click', '.klaviyo-form-KAF3vb button', function() {
        console.log('button clicked');
       // const $klaviyoForm = $('.ResetElements__Form-sc-8e6zl9-1');
        const $klaviyoForm = $('.klaviyo-form-KAF3vb');
        const emailId = $klaviyoForm.find('[name="email"]').val();
        const FirstName = $klaviyoForm.find('[aria-label="First Name"]').val();
        const LastName = $klaviyoForm.find('[aria-label="Last Name"]').val();
        const Birthday = $klaviyoForm.find('[aria-label="Birthday"]').val();
        const Gender = $klaviyoForm.find('#downshift-0-input').val();
        const ShoeSize = $klaviyoForm.find('#downshift-1-input').val();
        const Brand = $klaviyoForm.find('#downshift-3-input').val();
        const ShoeType = $klaviyoForm.find('#downshift-5-input').val();
        const Color = $klaviyoForm.find('#downshift-7-input').val();
        let validForm = true;
        if (emailId.length <= 0 || FirstName.length <= 0 || LastName <= 0 || Birthday <= 0 || Gender <= 0 || ShoeSize <= 0 || Brand <= 0 || ShoeType <= 0) {
          validForm = false;
        }
        setTimeout(function() {
          console.log('subscribed successfully');
          const invalidInput = $klaviyoForm.find('[aria-invalid="true"]').length;
          const formHTML = $klaviyoForm.html().toString().toLowerCase();
          console.log(formHTML.indexOf('thanks for subscribing'), invalidInput);
          if (formHTML.indexOf('thanks for subscribing') > -1 || invalidInput <= 0 || validForm === true) {
            const tagsData = `FirstName-${FirstName},LastName-${LastName},Birthday-${Birthday},Gender-${Gender},ShoeSize-${ShoeSize},Brand-${Brand},ShoeType-${ShoeType},Color-${Color},VIP`;
            console.log(tagsData);
            app._updateCustomerTags(emailId, tagsData);
          }
        }, 3000);
      });
    },

    _klaviyoSubscribe(list_ID, event) {
      const listId = list_ID;
      const email = $(event.currentTarget).closest('form').find('[data-email]').val();
      const $form = $(event.target).closest('form');
      const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

      if ($.trim($form.find('[data-email]').val()).length <= 0) {
        $form.find('[data-subscribeError]').html("<p>Email Address can't be empty</p>").css('color', '#ff1100').show();
      } else if (re.test($form.find('[data-email]').val()) === false) {
        $form.find('[data-subscribeError]').html('<p>Please provide valid Email Address.</p>').css('color', '#ff1100').show();
      } else {
        subscribe(listId, email, {
          $email: email,
        })
          .then((response) => {
            if (response.success) {
              // Yeahhhh Success
              $form.find('[data-subscribeError]').html('<p>Thank you for subscribing. We have sent you a confirmation email.</p>').css('color', '#28a745').show();
              $form.find('[data-email]').val('');
            } else {
              // Something went wrong, do something to notify the user.
              $form.find('[data-email]').css('borderColor', '#ff1100 !important');
              $form.find('[data-subscribe]').css('borderColor', '#ff1100 !important');
              $form.find('[data-subscribeError]').html(`<p>${response.errors[0]}</p>`).css('color', '#ff1100').show();
            }
          })
          .catch((error) => {
            // console.log(error);
            $form.find('[data-subscribeError]').html('Could not connect to the registration server. Please try again later.').show();
            throw error;
          });
      }
    },

    _flickitySlider() {
      if ($('body').find('.product').data('template') === 'product') {
        if (matchMedia('screen and (max-width: 768px)').matches) {
          const $carousel1 = $('[data-carousel-main]');
          const options1 = {
            adaptiveHeight: true,
            contain: true,
            wrapAround: true,
            freeScroll: false,
            accessibility: false,
            setGallerySize: true,
            prevNextButtons: true,
            pageDots: false,
            lazyLoad: 2,
            draggable: true,
            fullscreen: false,
            dragThreshold: 20,
            arrowShape: {
              x0: 10,
              x1: 60,
              y1: 50,
              x2: 60,
              y2: 45,
              x3: 15,
            },
          };
          const flkty1 = new Flickity($carousel1[0], options1);
          // flkty1.resize();

          const $carousel2 = $('.carousel-nav');
          const options2 = {
            asNavFor: '.carousel-main',
            adaptiveHeight: true,
            contain: true,
            cellAlign: true,
            wrapAround: false,
            freeScroll: false,
            accessibility: false,
            setGallerySize: true,
            prevNextButtons: true,
            pageDots: false,
            lazyLoad: 2,
            draggable: false,
            fullscreen: false,
            dragThreshold: 20,
            arrowShape: {
              x0: 10,
              x1: 60,
              y1: 50,
              x2: 60,
              y2: 45,
              x3: 15,
            },
          };
          const flkty2 = new Flickity($carousel2[0], options2);
        } else {
          const $carousel1 = $('[data-carousel-main]');
          const options1 = {
            adaptiveHeight: true,
            contain: true,
            wrapAround: true,
            freeScroll: false,
            accessibility: false,
            setGallerySize: true,
            groupCells: '100%',
            prevNextButtons: false,
            pageDots: false,
            lazyLoad: 2,
            draggable: false,
            fullscreen: false,
            dragThreshold: 20,
            arrowShape: {
              x0: 10,
              x1: 60,
              y1: 50,
              x2: 60,
              y2: 45,
              x3: 15,
            },
          };
          const flkty1 = new Flickity($carousel1[0], options1);
          // flkty1.resize();

          const $carousel2 = $('.carousel-nav');
          const options2 = {
            asNavFor: '.carousel-main',
            adaptiveHeight: true,
            contain: true,
            cellAlign: true,
            groupCells: '100%',
            wrapAround: false,
            freeScroll: false,
            accessibility: false,
            setGallerySize: true,
            prevNextButtons: true,
            pageDots: false,
            lazyLoad: 2,
            draggable: false,
            fullscreen: false,
            dragThreshold: 20,
            arrowShape: {
              x0: 10,
              x1: 60,
              y1: 50,
              x2: 60,
              y2: 45,
              x3: 15,
            },
          };
          const flkty2 = new Flickity($carousel2[0], options2);
          // flkty2.resize();
        }
      }
    },

    _prodVendor() {
      if (!$('#prod_vend').is(':visible')) {
        $('#prod_ven_title').css('display', 'block');
      }
    },

    _productOption() {
      $('.product-details-wrapper .selector-wrapper ul').each(function() {
        if (!$(this).has('li').length) {
          $(this).parent().hide();
        }
      });
    },

    _sidebarFilter() {
      $('body').on('click', '.filter_close', () => {
        $('#filtersCollapse').removeClass('show');
        app.isOverlayVisible = false;
        app._unlockScroll();
        $('html').removeClass('open_filter');
      });

      $('[data-filter_sidebar]').click(() => {
        app.isOverlayVisible = true;
        app._lockScroll();
        $('html').addClass('open_filter');
      });

      $('.collection-filters .card-body ul').each(function() {
        if (!$(this).has('li').length) {
          $(this).parent().parent().parent().hide();
        }
      });

      if ($('.nxt-results-count').length > 0) {
        $('.per-page-sort').hide();
      }
      setTimeout(() => {
        if ($('.nxt-results-count').length > 0) {
          $('.per-page-sort').hide();
        }
      }, 1000);

      setTimeout(() => {
        if ($('.nxt-results-count').length > 0) {
          $('.per-page-sort').hide();
        }
      }, 2000);

      $('#filtersCollapse .custom-checkbox').click(() => {
        $('.per-page-sort').hide();
      });

      if ($('body').find('#filtersCollapse .card-header').hasClass('collapsed')) {
        $('body').find('#filtersCollapse .card-header').removeClass('collapsed').siblings().addClass('collapse');
      }

      if ($('#filtersCollapse .custom-checkbox input:checked').length > 0) {
        if (window.location.hash) {
          $('.per-page-sort').attr('style', 'display:none !important');
          if ($('body').find('.row .col-12.col-sm-6.col-md-4.justify-content-around').length < 20) {
            $('body').find('.col-12.text-center.text-lg-left.mt-5').removeClass('col-md-6 col-lg-6');
          }
        }

        setTimeout(() => {
          $('.per-page-sort').attr('style', 'display:none !important');
          $('body').find('#filtersCollapse').addClass('nextFilter');
          $('body').find('input:checked').closest('[data-parent]').removeClass('collapse').siblings().addClass('collapsed');
        }, 2000);
      }

      $('body').on('click', '#filtersCollapse .card-body .custom-control', function() {
        setTimeout(() => {
          $('#filtersCollapse .card-header').removeClass('collapsed').siblings().addClass('collapse');
          if ($('body').find('#filtersCollapse .custom-checkbox input:checked').length > 0) {
            $('body').find('#filtersCollapse').addClass('nextFilter');
            $('body').find('input:checked').closest('[data-parent]').removeClass('collapse').siblings().addClass('collapsed');
            if ($('body').find('.row .col-12.col-sm-6.col-md-4.justify-content-around').length < 20) {
              $('body').find('.col-12.text-center.text-lg-left.mt-5').removeClass('col-md-6 col-lg-6');
            }
          }
          if (window.location.hash) {
            $('.per-page-sort').attr('style', 'display:none !important');
          }
        }, 1500);
      });

      $('body').on('click', '.nxt-refine-clearall', () => {
        setTimeout(() => {
          $('body').find('#filtersCollapse').removeClass('nextFilter');
          $('body').find('#filtersCollapse .card-header').removeClass('collapsed').siblings().addClass('collapse');
        }, 1300);
      });
    },

    _footer() {
      enquire.register('screen and (max-width:45em)', {
        match() {
          app.displaySliderMode = 'mobile';
          $('[data-footer-menutitle]').click(function(e) {
            $(this).parent('.footer-nav').parent('[data-footer-menulist]').siblings('[data-footer-menulist]').children('.footer-nav').removeClass('opened');
            $(this).parent('.footer-nav').parent('[data-footer-menulist]').siblings('[data-footer-menulist]').children('.footer-nav').children('.footer-nav-list').slideUp();

            if ($(this).parent('.footer-nav').hasClass('opened')) {
              $(this).parent('.footer-nav').removeClass('opened');
              $(this).next('.footer-nav-list').slideUp();
            } else {
              $(this).parent('.footer-nav').addClass('opened');
              $(this).next('.footer-nav-list').slideDown();
            }
          });
        },
        unmatch() {
          app.displaySliderMode = 'desktop';
          $('[data-footer-menutitle]').off('click');
        },
      });
    },

    _register(event) {
      const $form = $(event.target).closest('form');
      const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      $form.find('[data-subscribe]').val('Sending...');
      if ($.trim($form.find('[data-email]').val()).length <= 0) {
        $form.find('[data-subscribeError]').html("<p>Email Address can't be empty</p>").css('color', '#ff1100').show();
      } else if (re.test($form.find('[data-email]').val()) === false) {
        $form.find('[data-subscribeError]').html('<p>Please provide valid Email Address.</p>').css('color', '#ff1100').show();
      } else {
        $.ajax({
          type: $form.attr('method'),
          url: $form.attr('action'),
          data: $form.serialize(),
          cache: false,
          dataType: 'json',
          crossDomain: true,
          contentType: 'application/json; charset=utf-8',
          error(err) {
            $form.find('[data-subscribe]').val('Subscribe');
            $form.find('[data-subscribeError]').html('Could not connect to the registration server. Please try again later.').show();
          },
          success(data) {
            $form.find('[data-subscribe]').val('Subscribe');
            if (data.result === 'success') {
              // Yeahhhh Success
              $form.find('[data-subscribeError]').html('<p>Thank you for subscribing. We have sent you a confirmation email.</p>').css('color', '#28a745').show();
              $form.find('[data-email]').val('');
            } else {
              // Something went wrong, do something to notify the user.
              $form.find('[data-email]').css('borderColor', '#ff1100 !important');
              $form.find('[data-subscribe]').css('borderColor', '#ff1100 !important');
              $form.find('[data-subscribeError]').html(`<p>${data.msg}</p>`).css('color', '#ff1100').show();
            }
          },
        });
      }
      setTimeout(() => {
        $('[data-subscribeError]').hide();
      }, 7000);
    },

    _addLable() {
      $('.coll-items').each(function() {
        $("<span class='price mx-1 text-primary pr-2' >RUZE PRICE:</span>").insertBefore('.card .card-footer .text-left > .price');
      });
      $('.collection-filters .accordion div ul li .custom-control-input').each(function() {
        $('body').on('click', this, function() {
             setTimeout(() => {
                   $('.coll-items').each(function() {
                     if($('.price.mx-1.text-primary.pr-2').length > 0) {
                       // console.log("ruze pr lenth --->");
                     }
                     else{
                        //console.log("ruze price --->");
                       $( "<span class='price mx-1 text-primary pr-2' >RUZE PRICE:</span>" ).insertBefore( ".card .card-footer .text-left > .price" );
                     }
                   });
                 },4000);
           });
      });

      $('body').on('click', '.nxt-related a', () => {
        setTimeout(() => {
          $("<span class='price mx-1 text-primary pr-2' >RUZE PRICE:</span>").insertBefore('.coll-items .card .card-footer .text-left > .price');
        }, 4000);
      });
     // $('body').on('click', '#nxt-refines-container a,.pagination a', () => {
      $('body').on('click', '.pagination a', () => {
        setTimeout(() => {
          $("<span class='price mx-1 text-primary pr-2' >RUZE PRICE:</span>").insertBefore('.coll-items .card .card-footer .text-left > .price');
        }, 4000);
      });

      $('body').on('change', '#showItems,#sortBy', () => {
        setTimeout(() => {
          if($('.price.mx-1.text-primary.pr-2').length == 0) {
            $("<span class='price mx-1 text-primary pr-2' >RUZE PRICE:</span>").insertBefore('.coll-items .card .card-footer .text-left > .price');
          }
        }, 4000);
      });
    },
  },
};

export default customMethods;
